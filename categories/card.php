<div class="columns is-mobile">

  <div class="card-content card-background-color">
    <div class="media">
      <div class="media-content">
        <p class="title is-4">Name: <?= $category['name'] ?></p>
        <p class="title is-4">Description: <?= $category['description'] ?></p>
      </div>
    </div>

    <div class="content">
        <a class="button is-link" href="./categories/update.php?id=<?=$category['id']?>">Edit</a>
        <a class="button is-danger" href="./categories/delete.php?id=<?=$category['id']?>">Delete</a>
    </div>
  </div>
</div>