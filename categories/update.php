<?php
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = $_GET['id'] ?? '';
$category = $category_model->find($id)[0];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = $_POST['name'] ?? '';
    $description = $_POST['description'] ?? '';
    $category_model->update($id, $name, $description);
    return header("Location: /categories");
}

require_once './form.php';
 ?>